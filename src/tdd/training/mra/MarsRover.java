package tdd.training.mra;

import java.util.List;

public class MarsRover {
	
	private int planetX;
	private int planetY;
	private boolean planetMatrix[][];
	private String status;
	private int currentCellX;
	private int currentCellY;
	private String currentDirection;
	private final static String LANDING_STATUS = "(0,0,N)";
	private final static String NORTH = "N";
	private final static String SOUTH = "S";
	private final static String WEST = "W";
	private final static String EAST = "E";
	private List<String> planetObstacles;
	private String obstacles;

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		this.planetX = planetX;
		this.planetY = planetY;
		this.currentCellX = 0;
		this.currentCellY = 0;
		this.status = LANDING_STATUS;
		this.currentDirection = NORTH;
		this.planetObstacles = planetObstacles;
		this.planetMatrix = new boolean[ planetX ][ planetY ];
		this.obstacles= "";
		
		for( int i = 0 ;  i < planetObstacles.size() ; i++ ) {
			String currentOblstacle = planetObstacles.get( i );
			String stringCoordinates = currentOblstacle.substring(1, currentOblstacle.length() -1);
			
			String[] coordinates = stringCoordinates.split(",");
			
			int intCoordinatesX = Integer.parseInt( coordinates[0] );
			int intCoordinatesY = Integer.parseInt( coordinates[1] );
			
			this.planetMatrix[ intCoordinatesX ][ intCoordinatesY ] = true;
		}
		
		
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		if( this.planetMatrix[ x ][ y ] ) {
			return true;
		}
		return false;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		for (int i = 0; i < commandString.length(); i++){
		    Character commandChar = commandString.charAt( i );
		    String singleCommand =Character.toString( commandChar );
		    this.status = this.executeSingleCommand( singleCommand );
		}
		this.status += this.obstacles;
		return this.status;
	}
	
	
	
	public String executeSingleCommand(String commandString) throws MarsRoverException {
		if( commandString.equals( "" ) ) {
			return LANDING_STATUS;
		}
		else if( commandString.equals( "r" ) ) {
			this.turningRight();
			return this.status;
		}
		else if( commandString.equals( "l" ) ) {
			this.turningLeft();
			return this.status;
		}
		else if( commandString.equals( "f" ) ) {
			this.moveFoward();
			return this.status;
		}
		else if( commandString.equals( "b" ) ) {
			this.moveBackward();
			return this.status;
		}
		return null;
	}
	
	public void turningRight() {
		if( this.currentDirection.equals( NORTH )  ) {
			this.currentDirection = EAST;
			this.status =	this.status.replace( NORTH , EAST );
		}
		else if( this.currentDirection.equals( SOUTH )  ) {
			this.currentDirection = WEST;
			this.status =	this.status.replace( SOUTH , WEST );
		}
		else if( this.currentDirection.equals( WEST )  ) {
			this.currentDirection = NORTH;
			this.status =	this.status.replace( WEST , NORTH );
		}
		else if( this.currentDirection.equals( EAST )  ) {
			this.currentDirection = SOUTH;
			this.status =	this.status.replace( EAST , SOUTH );
		}
	}
	
	public void turningLeft() {
		if( this.currentDirection.equals( NORTH )  ) {
			this.currentDirection = WEST;
			this.status =	this.status.replace( NORTH , WEST );
		}
		else if( this.currentDirection.equals( WEST )  ) {
			this.currentDirection = SOUTH;
			this.status =	this.status.replace( WEST , SOUTH );
		}
		else if( this.currentDirection.equals( SOUTH )  ) {
			this.currentDirection = EAST;
			this.status =	this.status.replace( SOUTH , EAST );
		}
		else if( this.currentDirection.equals( EAST )  ) {
			this.currentDirection = SOUTH;
			this.status =	this.status.replace( EAST , SOUTH );
		}
	}
	
	public void moveFoward() {
		if( this.currentDirection.equals( NORTH )  ) {
			if( this.currentCellY == ( this.planetY - 1) ) {
				this.currentCellY = 0;
			}
			else {
				this.currentCellY++;
			}
		}
		else if( this.currentDirection.equals( WEST )  ) {
			if( this.currentCellX == 0 ) {
				this.currentCellX = ( this.planetX - 1 );
			}
			else {
				this.currentCellX--;
			}
		}
		else if( this.currentDirection.equals( SOUTH )  ) {
			if( this.currentCellY == 0 ) {
				this.currentCellY = ( this.planetY - 1 );
			}
			else {
				this.currentCellY--;
			}
		}
		else if( this.currentDirection.equals( EAST )  ) {
			if( this.currentCellX == ( this.planetX - 1 ) ) {
				this.currentCellX = 0;
			}
			else {
				this.currentCellX++;
			}
		}
		StringBuilder sbX = new StringBuilder();
		sbX.append("");
		sbX.append( this.currentCellX );
		String stringCurrentCellX = sbX.toString();
		StringBuilder sbY = new StringBuilder();
		sbY.append("");
		sbY.append( this.currentCellY );
		String stringCurrentCellY = sbY.toString();
		
		this.status =	"("+stringCurrentCellX+","+stringCurrentCellY+","+this.currentDirection+")";
		
		if( this.isObstacle( this.currentCellX , this.currentCellY ) ) {
			String obstacle = "("+this.currentCellX+","+this.currentCellY+")";
			this.moveBackward();
			if( !this.obstacles.contains( obstacle ) ) { 
				this.obstacles = this.obstacles +obstacle;
			}
		}
	}
	
	public void moveBackward() {
		if( this.currentDirection.equals( NORTH )  ) {
			if( this.currentCellY == 0 ) {
				this.currentCellY = (this.planetY -1);
			}
			else {
				this.currentCellY--;
			}
		}
		else if( this.currentDirection.equals( WEST )  ) {
			if( this.currentCellX == ( this.planetX - 1 ) ) {
				this.currentCellX = 0;
			}
			else {
				this.currentCellX++;
			}
		}
		else if( this.currentDirection.equals( SOUTH )  ) {
			if( this.currentCellY == ( this.planetY - 1 ) ) {
				this.currentCellY = 0;
			}
			else {
				this.currentCellY++;
			}
		}
		else if( this.currentDirection.equals( EAST )  ) {
			if( this.currentCellX == 0 ) {
				this.currentCellX = ( this.planetX - 1 );
			}
			else {
				this.currentCellX--;
			}
		}
		
		StringBuilder sbX = new StringBuilder();
		sbX.append("");
		sbX.append( this.currentCellX );
		String stringCurrentCellX = sbX.toString();
		StringBuilder sbY = new StringBuilder();
		sbY.append("");
		sbY.append( this.currentCellY );
		String stringCurrentCellY = sbY.toString();
		
		this.status =	"("+stringCurrentCellX+","+stringCurrentCellY+","+this.currentDirection+")";
		
		if( this.isObstacle( this.currentCellX , this.currentCellY ) ) {
			String obstacle = "("+this.currentCellX+","+this.currentCellY+")";
			this.moveFoward();
			if( !this.obstacles.contains( obstacle ) ) { 
				this.obstacles = this.obstacles +obstacle;
			}
		}
	}

	public boolean isObstacle( int x, int y ) {
		return this.planetMatrix[x][y];
	}
	
}
