package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.*;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void testContainsObstacleAtGivenCell() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>() ;
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");		
		
		MarsRover marsRover = new MarsRover( 10, 10,  planetObstacles );
		
		assertTrue( marsRover.planetContainsObstacleAt(4,  7) );
	}
	
	@Test
	public void testExectuteCommand() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>() ;
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");		
		String command = "";
		MarsRover marsRover = new MarsRover( 10, 10,  planetObstacles );
		
		assertEquals( "(0,0,N)", marsRover.executeCommand( command ) );
	}
	
	@Test
	public void testTurnRight() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>() ;
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");		
		String command = "r";
		MarsRover marsRover = new MarsRover( 10, 10,  planetObstacles );
		
		assertEquals( "(0,0,E)", marsRover.executeCommand( command ) );
	}

	@Test
	public void testTurnLeft() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>() ;
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");		
		String command = "l";
		MarsRover marsRover = new MarsRover( 10, 10,  planetObstacles );
		
		assertEquals( "(0,0,W)", marsRover.executeCommand( command ) );
	}
	
	@Test
	public void testMovingForward() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>() ;
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");		
		String command = "f";
		MarsRover marsRover = new MarsRover( 10, 10,  planetObstacles );
		
		assertEquals( "(0,1,N)", marsRover.executeCommand( command ) );
	}
	
	@Test
	public void testMovingBackward() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>() ;
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");		
		String command = "f";
		MarsRover marsRover = new MarsRover( 10, 10,  planetObstacles );
		
		String status = marsRover.executeCommand( command );
		command = "b";
		
		assertEquals( "(0,0,N)", marsRover.executeCommand( command ) );
	}
	
	@Test
	public void testMovingCombined() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>() ;
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");		
		String command = "ffrff";
		MarsRover marsRover = new MarsRover( 10, 10,  planetObstacles );
		
		assertEquals( "(2,2,E)", marsRover.executeCommand( command ) );
	}
	
	@Test
	public void testWrapping() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>() ;
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");		
		String command = "b";
		MarsRover marsRover = new MarsRover( 10, 10,  planetObstacles );
		
		assertEquals( "(0,9,N)", marsRover.executeCommand( command ) );
	}
	
	@Test
	public void testSingleObstacle() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>() ;
		planetObstacles.add("(2,2)");		
		String command = "ffrfff";
		MarsRover marsRover = new MarsRover( 10, 10,  planetObstacles );
		
		assertEquals( "(1,2,E)(2,2)", marsRover.executeCommand( command ) );
	}
	
	@Test
	public void testMultipleObstacle() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>() ;
		planetObstacles.add("(2,2)");		
		planetObstacles.add("(2,1)");
		String command = "ffrfffrflf";
		MarsRover marsRover = new MarsRover( 10, 10,  planetObstacles );
		
		assertEquals( "(1,1,E)(2,2)(2,1)", marsRover.executeCommand( command ) );
	}
	
	@Test
	public void testWrappingObstacle() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>() ;
		planetObstacles.add("(0,9)");	
		String command = "b";
		MarsRover marsRover = new MarsRover( 10, 10,  planetObstacles );
		
		assertEquals( "(0,0,N)(0,9)", marsRover.executeCommand( command ) );
	}
	
	@Test
	public void testTourAroundPlanet() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>() ;
		planetObstacles.add("(2,2)");
		planetObstacles.add("(0,5)");
		planetObstacles.add("(5,0)");
		String command = "ffrfffrbbblllfrfrbbl";
		MarsRover marsRover = new MarsRover( 6, 6,  planetObstacles );
		
		assertEquals( "(0,0,N)(2,2)(0,5)(5,0)", marsRover.executeCommand( command ) );
	}
	
}
